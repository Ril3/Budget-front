For Back-End I've used:
- NodeJS
- Express server
- MySQL
- Babel
- Rooter/Controler
- Supertest
- Dotenv-flow
  
For Front-End I've used:
- ReactJS
- React router
- Axios
- CSS


The back-end of the project is deployed in Heroku on this URL: https://protected-refuge-09848.herokuapp.com
The Front-end of this project is deplyed on Netlify on this URL(This is the homepage of the app aswell): https://epic-haibt-682323.netlify.app/.

Description:
It is a simple application that allows you to:
-Display all operations
-There is a search bar that allows you to search and sort the operations
    -Sort and filter operations by category
    -Sort and filter operations by date
-Display total amount of money that you've spent (Total is dynamic and it can do the sum of all operations or sum of one category or sum of one month.)
-You can easy delete operaion from data base with just one click on the button x that is displayed on every operation.
-You can also add operation (There is Add Operation page that contains form for you to fill up and submit. It'll send your data to data base after you submit)



![alt text](public/images/Budgetall.png) ;
![alt text](public/images/BudgetADD.png) ;