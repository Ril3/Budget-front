import { Switch, Route} from "react-router-dom";
import './App.css';
import Navbar from "./Components/Navbar";
import AddOperation from "./Pages/AddOperation";
import Home from "./Pages/Home";




const App = ()=> {
  return (
    <div className="App">

      <Navbar />
        <Switch>
          <Route exact path='/'>
            <Home />
          </Route>

          <Route exact path='/AddOperation'>
              <AddOperation />
          </Route>

        </Switch>
    </div>
  );
}

export default App;
