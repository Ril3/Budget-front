import axios from "axios";
import React, { useState } from "react";
import Form from "../Components/Form";
import './AddOperation.css'

export default function AddOperation(){

  const [operations, setOperations] = useState([]);

  async function addEvent(oneoperation){
    const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/operations/add', oneoperation)
    setOperations([
        ...operations,
        response.data
    ]);
    alert("You've successfully added new operation")
  }


    return (
      <div className="forminDiv">
         <Form onFormSubmit={addEvent}/>
         
      </div>
    )
  }