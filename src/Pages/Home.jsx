import axios from "axios";
import React, { useEffect, useState } from "react";
import SearchBar from "../Components/Search";
import { Operation } from "../Components/Operation";
import './Home.css';





export default function Home(){

  const [operations, setOperations] = useState([]);

  async function fetchOperation(){
    const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/operations/all')
    setOperations(response.data)
  }

  async function deleteOperation(id) {
    await axios.delete(process.env.REACT_APP_SERVER_URL+'/api/operations/delete/' + id);
    setOperations(
        operations.filter(item => item.id !== id)
    );
}


  useEffect(()=>{
    fetchOperation()
  }, [])

//Search bar
  const { search } = window.location;
  const query = new URLSearchParams(search).get('s');
  const [searchQuery, setSearchQuery] = useState(query || '');
  
//Checking if anything is writen in input. If yes filter operations and return filtered operations if not return as it was before.
  const filterOperations = (operations, query) => {
            if (!query) {
                return operations;
            } else if(!isNaN(query)){
              return operations.filter((operation) => {
                const operationName = operation.date;
                return operationName.includes(query);
            });
            }else{
                return operations.filter((operation) => {
                  const operationName = operation.category;
                  return operationName.includes(query);
              });
            }
  
    
  };
  const filteredOperations = filterOperations(operations, query);

// Total
  function Price(){
    let total = 0;
    for (const element of filteredOperations) {
        total += element.total


    }return total
}

    return (
      <div className="bigContainer">
         <p className="total"> Total :<span>{Number.parseFloat(Price()).toFixed(2)}€</span> </p>

          <SearchBar searchQuery={searchQuery}
                     setSearchQuery={setSearchQuery}
                     />
         
          <section className="container">
                {filteredOperations.map(item =>
                    <Operation key={item.id} 
                              operation={item} 
                              onDelete={deleteOperation} 
                              /> 
                )}
          </section>
         
      </div>
    )
  }