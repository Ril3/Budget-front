// import { useHistory } from "react-router-dom";
import "./Search.css"

export default function SearchBar({ searchQuery, setSearchQuery }){

//       const history = useHistory();
//       const onSubmit = e => {
//              history.push(`?s=${query}`)
//              e.preventDefault()
//          };

    return (
        <form action="/" method="get" autoComplete="off" className="searchforma">
            <label htmlFor="header-search">
                <span className="visually-hidden">Search operation</span>
            </label>
            <input
                value={searchQuery}
                onInput={e => setSearchQuery(e.target.value)}
                type="text"
                id="header-search"
                placeholder="Search operation"
                name="s" 
            />
            <button type="submit" className="btnSearch">Search</button>
        </form>
    )
}