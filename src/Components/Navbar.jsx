import React from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";


export default function Navbar(){
        return (
            <nav className="navBar">
                <img src="./images/budget.png" alt="Logo" />
                <ul>
                    <li>
                        <Link className="link" to="/">Operations</Link>
                    </li>
                    <li>
                        <Link className="link" to="/AddOperation">Add Operation</Link>
                    </li>
                </ul>
            </nav>
    )
}