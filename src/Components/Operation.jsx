import './Operation.css'


export function Operation({ operation, onDelete }) {

    return (
      
            <article className="operationCard">
                <button className='btn-delete' onClick={()=> onDelete(operation.id)}>X</button>
                <div className="data">
                    <p>{operation.operation}</p>
                    <p>Date: {operation.date}</p>
                    <p>Category: {operation.category}</p>
                    <p className='price'>Spent: {operation.total}</p>
                </div>
            </article>
    );
}