import { useState } from "react";


export default function Form({onFormSubmit}){

    const [oneoperation, setOperation] = useState({
        date: "",
        operation: "",
        category: "",
        total: ""
      });

      function handleChange(event){
      
        setOperation({
            ...oneoperation,
            [event.target.name]: event.target.value
        });
  }

  function handleSubmit(event) {
    event.preventDefault();
   onFormSubmit(oneoperation)
}

  return (
    <form className="forma" onSubmit={handleSubmit}>
          <label>Date :</label>
          <input type="date" name="date" onChange={handleChange} value={oneoperation.date} />
          <label>Operation:</label>
          <input required type="text" name="operation" onChange={handleChange} value={oneoperation.operation} />
          <label>Category:</label>
          <input required type="text" name="category" onChange={handleChange} value={oneoperation.category} />
          <label>Total:</label>
          <input required type="number" name="total" onChange={handleChange} value={oneoperation.total}/>
          <button>Submit</button>
    </form>
  )
}